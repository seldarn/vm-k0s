# vm-k0s



## Info

The goal of this project is to offer a lightweight Kubernetes environment for a developer on a windows machine.

For that, it :
- uses Vagrant to create an Ubuntu VM with VirtualBox
- installs k0s in the VM
- add a kubeconfig file in the .kube directory of your windows user ( you can then use [Lens](https://k8slens.dev/) to connect to the cluster)
- create a dev namespace
- install helm
- _install a docker repository (to be done )_




## How to use it

```
vagrant up
```
**Prerequits: Vagrant must be installed on windows**
