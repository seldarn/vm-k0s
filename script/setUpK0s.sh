#!/bin/sh

IP_ADRESS=$1
KUBE_CONFIG_DIRECTORY=$2
NAMESPACE=$3

#mkdir -p /etc/k0s
#chmod 770 /etc/k0s
chmod +x /usr/local/bin/k0s
#k0s install controller --enable-worker -c /etc/k0s/k0s-config.yaml
sudo k0s install controller --single --debug
#spleep is needed 
sleep 10
echo "k0s is now starting"
sudo k0s start --debug
sleep 10
echo "k0s status"
k0s status 
k0s kubeconfig admin > "$KUBE_CONFIG_DIRECTORY"
sed -i "s/10.0.2.15/$IP_ADRESS/" "$KUBE_CONFIG_DIRECTORY"

echo "alias k='sudo k0s kubectl'" >> .bashrc
alias k='sudo k0s kubectl'
sleep 60
k get nodes
#creation du namespace de dev
k create ns "$NAMESPACE" 
#k taint nodes --all node-role.kubernetes.io/master-
