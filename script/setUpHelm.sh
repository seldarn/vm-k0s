#!/bin/sh

KUBE_CONFIG_DIRECTORY=$1
NAMESPACE=$2
KUBE_YAML_DIRECTORY=$3

#installation de helm
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

#ajout du registry docker
helm repo add twuni https://helm.twun.io
helm upgrade --install docker-registry twuni/docker-registry \
    --kubeconfig "$KUBE_CONFIG_DIRECTORY" \
    --namespace "$NAMESPACE" 

helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace \
  --kubeconfig "$KUBE_CONFIG_DIRECTORY"

k apply -f "$KUBE_YAML_DIRECTORY/ingress.yaml" -n "$NAMESPACE"

k port-forward --namespace=ingress-nginx service/ingress-nginx-controller 8181:80 --address=192.168.15.211